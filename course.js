let params = new URLSearchParams(window.location.search)

let courseId = params.get("courseId")

// console.log(params.get("courseId"))

fetch(`http://localhost:4000/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	console.log(data)
})

/*
ACTIVITY:

Using fetch, show the details of the page's specific course in the console.

When done, create an a4 folder in activities, copt course.js in that folder, then push to Gitlab and paste the link in Boodle.

*/